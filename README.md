# Geruechtegenerator

Geruechtegenerator is a web based project that creates *rumors (Gerüchte)* from
a database (currently a .json file).  The database contains rumors and names.

A special datatype allows one to adapt sentences so certain words match the
gender of the selected name(s).

# Include in you website

These two lines of code should suffice to include fresh rumors into your own
website:

```
<div id="geruecht">Placeholder if you want</div>
<script type="text/javascript" src="path|url to scriptfile"></script>
```

Either get the script from here (frontend/js/gg3frontend.js) or include
the link to the script from the Geruechtegenerator's website (if you trust us):

https://plagi.at/geruecht/js/gg3frontend.js

If you add an `onclick="nextgeruecht()"` attribute to the element with
`id="geruecht"` you can let your users get new rumors as often as they want ;)

# Roadmap

* Create a REST API based on [Tornado] (25% done)
* Write a Frontend using [React] (25% done)

[Tornado]:https://www.tornadoweb.org/en/stable/
[React]:https://reactjs.org/
