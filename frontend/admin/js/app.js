// $(document).foundation();

/*
  Switch actions
*/
$('.unmask').on('click', function(){

  if($(this).prev('input').attr('type') == 'password')
    changeType($(this).prev('input'), 'text');

  else
    changeType($(this).prev('input'), 'password');

  return false;
});

function changeType(x, type) {
  if(x.prop('type') == type)
  return x; //That was easy.
  try {
    return x.prop('type', type); //Stupid IE security will not allow this
  } catch(e) {
    //Try re-creating the element (yep... this sucks)
    //jQuery has no html() method for the element, so we have to put into a div first
    var html = $("<div>").append(x.clone()).html();
    var regex = /type=(\")?([^\"\s]+)(\")?/; //matches type=text or type="text"
    //If no match, we add the type attribute to the end; otherwise, we replace
    var tmp = $(html.match(regex) == null ?
      html.replace(">", ' type="' + type + '">') :
      html.replace(regex, 'type="' + type + '"') );
    //Copy data from old element
    tmp.data('type', x.data('type') );
    var events = x.data('events');
    var cb = function(events) {
      return function() {
            //Bind all prior events
            for(i in events)
            {
              var y = events[i];
              for(j in y)
                tmp.bind(i, y[j].handler);
            }
          }
        }(events);
        x.replaceWith(tmp);
    setTimeout(cb, 10); //Wait a bit to call function
    return tmp;
  }
}

//
// admin interface
//

var appdata = null;

//
// REST functions
//
function startsession() {
    console.log('GG3 admin interface loading');

    fetch('https://localhost:8888/session', {
        credentials: 'include',
        method: 'GET',
        mode: 'cors',
        headers:  {
            'Content-Type': 'text/plain'
        }
    })
    .then(function(response) {
        if (response.ok) {
            // console.log('response received');
            // appdata = response.json();
            return response.json();
        }
        throw new Error('Network error (response not ok)');
    })
    .then(function(data) {
        appdata = data;
        initInterface();
        return;
    })
    .catch(function(error) {
        // document.getElementById('geruecht').innerHTML = 'addgeruecht Fehler: ' + error;
        console.log('addgeruecht error:  ' + error);
        return;
    })
}

function login() {
    var name = $('#name').val();
    var password = $('#password').val();
    console.log('login name=' + name + ', password=' + password);
    $.ajax({
        type: 'POST',
        // datatype: 'json',
        xhrFields: { withCredentials: true},
        crossdomain: true,
        url: 'https://localhost:8888/login/user/' + name + '/password/' + password,
        success: function(data, status, jqXHR) {
            appdata = JSON.parse(data);
            initInterface();
            console.log('user ' + name + ' ' + appdata.message);
        },
        error: function(jqXHR, status) {
            console.log('Error');
        }
    })
    return false;
}

//
// call the editor for obj.meta.type
//
function editorModalWidget(obj) {
    $('#editor-content').append(
        $('<div/>').attr('id', 'editor-container').addClass('clearfix')
    );
    $('#editor-content').append(
        $('<div/>').addClass('callout').attr('id', 'editor-status').hide()
    );
    $('#editor-content').append(
        $('<div/>').addClass('button-group callout').attr('id', 'editor-buttons')
    );
    $('#editor-content').append(
        $('<form/>').attr('id', 'editor-form')
    );
    stateFieldWidget(obj);

    // add type specific parts
    if (obj.meta.type == 'rumor') {
        rumorEditorModalWidget(obj);
    }
    else if (obj.meta.type == 'name') {
        nameEditorModalWidget(obj);
    }

    // add a save button
    buttonWidget('speichern', 'primary', 'button', '#editor-buttons', function() {
        try {
            // TODO this has to be checked/tested for names!
            obj.value = JSON.parse($('#json').val());
            saveRumor(obj);
            $('#editor-modal').foundation('close');
            $('#editor-content').remove();
            $('#rumortab').remove();
            $('#nametab').remove();
            makeTabs();
        } catch(e) {
            console.log('error in json');
            $('#editor-status').addClass('alert').text('Fehler im JSON').show();
        }
    })

    // add an apply button
    buttonWidget('übernehmen', 'primary', 'button', '#editor-buttons', function() {
        try {
            obj.value = JSON.parse($('#json').val());
            $('#editor-content').remove();
            $('#editor-modal').append(
                $('<div/>').attr('id', 'editor-content')
            )
            editorModalWidget(obj);
            $('#editor-status').text('Änderungen übernommen');
        } catch(e) {
            console.log('error in json');
            $('#editor-status').addClass('alert').text('Fehler im JSON').show();
        }
    })

    // add a cancel button
    buttonWidget('abbrechen', 'primary', 'button', '#editor-buttons', function() {
        $('#editor-modal').foundation('close');
        $('#editor-content').remove();
    })

    appendMeta(obj.meta);
}

//
// editor for a name
//
function nameEditorModalWidget(name) {
    console.log('edit name button clicked');
}

//
// editor for a rumor
//
function rumorEditorModalWidget(rumor) {
    for (i = 0; i < rumor.value.length; i++) {
        $('#editor-container').append(
            $('<span/>').text(printWord(rumor.value[i])).addClass('word float-left')
        );
    }

    $('#editor-form').append(
        $('<textarea/>').text(JSON.stringify(rumor.value, null, '    ')).attr('rows', '8').attr('cols', '40').attr('id', 'json')
    );
}

//
// widget for object's state display/switch
//
function stateFieldWidget(obj) {
    $('#editor-form').append(
        $('<div/>').addClass('callout').append(
            $('<fieldset/>').addClass('cell large-6').append(
                $('<legend/>').text('Sichtabarkeit')
            ).append(
                $('<input/>').attr('type', 'radio').attr('name', 'state').val('0').attr('id', 'privat')
            ).append(
                $('<label/>').attr('for', 'privat').text('privat').addClass('label alert')
            ).append(
                $('<input/>').attr('type', 'radio').attr('name', 'state').val('1').attr('id', 'public')
            ).append(
                $('<label/>').attr('for', 'public').text('öffentlich').addClass('label primary')
            ).append(
                $('<input/>').attr('type', 'radio').attr('name', 'state').val('2').attr('id', 'deleted')
            ).append(
                $('<label/>').attr('for', 'deleted').text('gelöscht').addClass('label secondary')
            )
        )
    )
}

//
// widget for a button
//
function buttonWidget(text, classname, type, appendto, func) {
    $('<button/>').attr('type', type).addClass('button ' + classname).text(text).on('click', func).appendTo(appendto);
    console.log('button added');
}

function saveRumor(rumor) {
    var numberofnames = 0;
    for (var i = 0; i < rumor.value.length; i++) {
        if (rumor.value[i].type == 'name') {
            numberofnames++;
        }
    }
    rumor.meta.lastmod = new Date() / 1000;
    rumor.meta.numberofnames = numberofnames;
    rumor.meta.lastmod_by = appdata.username;
    rumor.meta.state = Number($('input[name=state]:checked', '#editor-form').val());

    for (var i = 0; i < appdata.rumor.length; i++) {
        if (appdata.rumor[i].meta.id == rumor.meta.id) {
            appdata.rumor[i] = rumor;
        }
    }
    appdata.modified = true;
}

function seconds2date(seconds) {
    var t = new Date(1970,0,1);
    t.setSeconds(seconds);
    return t;
}

function appendMeta(meta) {
    $('#editor-content').append(
        $('<div/>').addClass('callout').append(
            $('<p/>').text('zuletzt bearbeitet von: ' + meta.lastmod_by)
        ).append(
            $('<p/>').text('zuletzt bearbeitet am/um: ' + seconds2date(meta.lastmod))
        ).append(
            $('<p/>').text('erzeugt am/um: ' + seconds2date(meta.created))
        ).append(
            $('<p/>').text('hinzugefügt am/um: ' + seconds2date(meta.added))
        ).append(
            $('<p/>').text('ID: ' + meta.id)
        )
    );
    // $('#editor-content').append(
    //     $('<pre/>').text(JSON.stringify(meta, null, 4))
    // );

    // check the correct state input radio field
    switch(meta.state) {
        case 0:
        $('#privat').attr('checked', 'checked');
        break;
        case 1:
        $('#public').attr('checked', 'checked');
        break;
        case 2:
        $('#deleted').attr('checked', 'checked');
        break;
    }
}

function printWord(word) {
    switch(word.type) {
        case 'static':
        return word.word;
        case 'word':
        return word.female + '/' + word.male;
        case 'name':
        return 'Name';
        case 'punctuation':
        return word.word;
    }
}

function setState(obj) {
    if (obj.meta.state == 0) {
        return $('<span/>').addClass('alert label').text('privat');
    }
    if (obj.meta.state == 1) {
        return $('<span/>').addClass('primary label').text('öffentlich');
    }
    if (obj.meta.state == 2) {
        return $('<span/>').addClass('secondary label').text('gelöscht');
    }
    return $('<span/>').addClass('warning label').text('unbekannt');
}

function initInterface() {
    if (appdata.status == 'success') {
        console.log('auth session started');
        editorWidget();
    } else {
        console.log('anon session started');
        loginWidget();
    }
    $(document).foundation();
    updateStatusBar('Hello ' + appdata.username + '!', 'primary');
}

function loginWidget() {
    $('#login-widget').remove();
    $('#editor-widget').remove();
    $('#container').removeClass('large-12').addClass('large-6');
    $('#container').append(
        $('<div/>').attr('id', 'login-widget').append(
            $('<div/>').addClass('callout').append(
                $('<h3/>').text('Anmelden')
            )
        ).append(
            $('<form/>').addClass('show-password').attr('id', 'login').append(
                $('<label/>').attr('for', 'name').text('Name')
            ).append(
                $('<input/>').attr('type', 'text').attr('placeholder', 'Name eingeben').attr('id', 'name')
            ).append(
                $('<div/>').addClass('password-wrapper').append(
                    $('<label/>').attr('for', 'password').text('Passwort')
                ).append(
                    $('<input/>').attr('type', 'password').attr('placeholder', 'Passwort eingeben').attr('id', 'password').addClass('password')
                ).append(
                    $('<button/>').addClass('unmask').attr('type', 'button').text('Unmask')
                )
            ).append(
                $('<button/>').addClass('button').attr('type', 'submit').text('Anmelden')
            ).on('submit', login)
        )
    );
}

function editorWidget() {
    $('#login-widget').remove();
    $('#editor-widget').remove();
    $('#container').removeClass('large-6').addClass('large-12');
    $('#container').append(
        $('<div/>').attr('id', 'editor-widget')
    );
    statusbarWidget('#editor-widget');
    toolbarWidget('#editor-widget');
    tabsWidget('#editor-widget');
    // $('#editor-widget').foundation();
}

function tabsWidget(appendto) {
    $('#tabs').remove();
    $('<div/>').attr('id', 'tabs').append(
        $('<ul/>').addClass('tabs').attr('data-tabs', '').attr('id', 'tablist').append(
            $('<li/>').addClass('tabs-title is-active').append(
                $('<a/>').attr('href', "#rumortab").attr('aria-selected', 'true').text('Gerüchte')
            )
        ).append(
            $('<li/>').addClass('tabs-title').append(
                $('<a/>').attr('href', "#nametab").text('Namen')
            )
        )
    ).append(
        $('<div/>').addClass('tabs-conetent').attr('data-tabs-content', 'tablist').attr('id', 'tabscontent')
    ).appendTo($(appendto));
    makeTabs();
}

function statusbarWidget(appendto) {
    $('<div/>').addClass('callout').attr({'id': 'statusbar', 'data-closable': ''}).append(
        $('<button/>').addClass('close-button').attr({'data-close': '', 'type': 'button'}).append(
            $('<span/>').text('x').attr('aria-hidden', 'true')
        )
    ).append(
        $('<p/>').attr('id', 'statusbar-text')
    ).appendTo($(appendto));
}

//
// use to update the statusbar (set text and style/classname)
//
function updateStatusBar(text, classname) {
    $('#statusbar-text').text(text);
    $('#statusbar').toggleClass(classname).toggle(true);
}

function toolbarWidget(appendto) {
    $('<div/>').addClass('callout').attr('id', 'toolbar').append(
        $('<div/>').addClass('button-group').attr('id', 'button-toolbar')
    ).appendTo($(appendto));
    saveButton('#button-toolbar');
    logoutButton('#button-toolbar');
    filterWidget('#toolbar');
}

function logoutButton(appendto) {
    $('<button/>').addClass('button').text('abmelden').on('click', function() {
        document.cookie = 'user=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;'
        appdata = null;
        loginWidget();
    }).appendTo($(appendto));
}

function saveButton(appendto) {
    $('<button/>').addClass('button').text('speichern').on('click', function() {
        console.log('Button save clicked');
        updateStatusBar('noch nicht implementiert!', 'alert');
        window.setTimeout(function() {
            updateStatusBar('Hello ' + appdata.username + '!', 'alert');
        }, 5000);
    }).appendTo(appendto);
}

function filterWidget(appendto) {
    $('<div/>').attr('id', 'filter-toolbar').addClass('top-bar').appendTo(appendto);
    filterSelectState('#filter-toolbar');
    filterText('#filter-toolbar');
    resultCounter('#filter-toolbar');
}

function resultCounter(appendto) {
    $('<label/>').text('Ergebnisse').append(
        $('<input/>').attr('type', 'text').attr('id', 'result-counter').attr('readonly', true)
    ).appendTo(appendto);
}


function filterSelectState(appendto) {
    $('<label/>').text('nach Sichtabarkeit filtern').append(
        $('<select/>').attr('id', 'state-filter').append(
            $('<option/>').attr('name', 'all').val(-1).text('alle')
        ).append(
            $('<option/>').attr('name', 'public').val(1).text('öffentlich')
        ).append(
            $('<option/>').attr('name', 'private').val(0).text('privat')
        ).append(
            $('<option/>').attr('name', 'deleted').val(2).text('gelöscht')
        ).on('change', function() {
            applyFilter();
        })
    ).appendTo(appendto);
}

function filterText(appendto) {
    $('<label/>').text('nach Schlüsselwort filtern').append(
        $('<input/>').attr('type', 'text').attr('placeholder', 'Schlüsselwort (mindestens 3 Zeichen)').attr('id', 'text-filter').on('keyup', function() {
            applyFilter();
        })
    ).appendTo(appendto);
}

function applyFilter() {
    var visible = $('.tabs-panel:visible').attr('id');
    var re = /tab/;
    var type = visible.replace(re, '');
    $('#' + visible).remove();
    $('#tabscontent').append($('<div/>').attr('id', visible).addClass('tabs-panel is-active'));
    tableWidget(type);
}

function makeTabs() {
    $('#tabscontent').append(
        $('<div/>').addClass('tabs-panel is-active').attr('id', 'rumortab')
    ).append(
        $('<div/>').addClass('tabs-panel').attr('id', 'nametab')
    );
    tableWidget('rumor');
    tableWidget('name');
}

function filter(obj, statefilter, re, tflen) {
    if (statefilter == -1 || statefilter == obj.meta.state) {
        if (tflen < 3) {
            return obj;
        }
        return textFilter(obj, re);
    }
    return null;
}

function textFilter(obj, re) {
    var searchable = '';

    if (obj.meta.type == 'name') {
        searchable = obj.value;
    }
    else if (obj.meta.type == 'rumor') {
        for (var i = 0; i < obj.value.length; i++) {
            if (obj.value[i].type == 'static') {
                searchable += obj.value[i].word + ' ';
            } else if (obj.value[i].type == 'word') {
                searchable += obj.value[i].female + ' ' + obj.value[i].male + ' ';
            }
        }
    }
    return searchable.match(re);
}

function tableHeadWidget(headtype, appendto) {
    var head = '';

    if (headtype == 'rumor') {
        head = 'Gerücht';
    } else if (headtype == 'name') {
        head = 'Name';
    }

    $('<thead/>').append(
        $('<tr/>').append(
            $('<th/>').attr('width', '20').text('#')
        ).append(
            $('<th/>').text(head)
        ).append(
            $('<th/>').attr('width', '75').text('Sichtbarkeit')
        ).append($('<th/>').attr('width', '50').text('Action'))
    ).appendTo(appendto);
}

//
// construct a table for 'listtype' using filter
//
function tableWidget(listtype) {
    var appendto = '#' + listtype + 'tab';
    var tableid = listtype + '';
    var tablebodyid = tableid + 'body';

    $('<table/>').attr('id', tableid).appendTo(appendto);
    tableHeadWidget(listtype, '#' + tableid);
    $('<tbody/>').attr('id', tablebodyid).appendTo('#' + tableid);
    var statefilter = Number($('#state-filter').val());
    var textfilter = $('#text-filter').val();
    var re = new RegExp(textfilter, 'i');
    var index = 0;

    for (var i = 0; i < appdata[listtype].length; i++) {
        if (filter(appdata[listtype][i], statefilter, re, textfilter.length) != null) {
            tableRowWidget(listtype, appdata[listtype][i], ++index, '#' + tablebodyid);
        }
    }

    if ($('.tabs-panel:visible').attr('id') == listtype + 'tab') {
        $('#result-counter').val(index);
    }
}

function tableRowWidget(type, obj, index, appendto) {
    var tablerow = $('<tr/>');
    var tabledata = $('<td/>');
    var item = '';

    if (type == 'rumor') {
        for (var i = 0; i < obj.value.length; i++) {
            item += printWord(obj.value[i]);
            item += ' ';
        }
    }
    else if (type == 'name') {
        item = obj.value;
    }

    $('<tr/>').append(
        $('<td/>').text(index)
    ).append(
        $('<td/>').text(item)
    ).append(
        $('<td/>').append(setState(obj))
    ).append(
        editButton(obj)
    ).appendTo(appendto);
}

//
// a button that creates an eidtor editor modal
//
function editButton(obj) {
    var button = $('<a/>').text('editieren').on('click', function() {
        $('#editor-modal').append($('<div/>').attr('id', 'editor-content'));
        $('#editor-modal').append(
            $('<button/>').addClass('close-button').attr('id', 'editor-close-button').attr('type', 'button').append(
                $('<span/>').text('x').attr('aria-hidden', 'true')));
                editorModalWidget(obj);
        $('#editor-close-button').on('click', function() {
            $('#editor-modal').foundation('close');
            $('#editor-content').remove();
            $('#editor-close-button').remove();
        });
        $('#editor-modal').foundation('open');
    });
    return $('<td/>').append(button);
}

startsession();
// EOF
