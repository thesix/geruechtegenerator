// Javascript for ggv3 frontend
// Copyright GPL v3 institut hofos 2019

var getggurl = '//api.plagi.at/get';
var addggurl = '//api.plagi.at/add';

function isFetchSupported() {
    return 'fetch' in window;
}

function nextgeruecht() {
    if (isFetchSupported() == false) {
        document.getElementById('geruecht').innerHTML = 'You browser does not support fetch.';
        return;
    }
    
    fetch(getggurl, {
        method: 'GET'
    })
    .then(function(response) {
        if (response.ok) {
            return response.json();
        }
        throw new Error('Network error (response not ok)');
    })
    .then(function(data) {
        document.getElementById('geruecht').innerHTML = data.payload;
    })
    .catch(function(error) {
        document.getElementById('geruecht').innerHTML = 'API Fehler: ' + error;
        console.log('API error:  ' + error);
    })
}

function addgeruecht() {
    var object = {}
    object.type = 'geruecht';
    object.payload = document.getElementById('newgeruecht').value;
    if (object.payload.length < 20) {
        input = document.getElementById('newgeruecht');
        input.className = 'form-control is-invalid';
        input.focus();
        document.getElementById('newgeruechtHelp').className = 'form-text text-danger';
        return false;
    }
    fetch(addggurl, {
        credentials: 'include',
        method: 'PUT',
        mode: 'cors',
        body: JSON.stringify(object),
        headers:  {
            'Content-Type': 'application/json; charset=utf8'
        }
    })
    .then(function(response) {
        if (response.ok) {
            return response.json();
        }
        throw new Error('Network error (response not ok)');
    })
    .then(function(data) {
        window.alert('Danke!  Das Gerücht wird überprüft und bei Gefallen in die Sammlung aufgenommen.');
        input = document.getElementById('newgeruecht');
        input.value = "";
        input.className = 'form-control';
        input.focus();
        document.getElementById('newgeruechtHelp').className = 'form-text';
    })
    .catch(function(error) {
        document.getElementById('geruecht').innerHTML = 'addgeruecht Fehler: ' + error;
        console.log('addgeruecht error:  ' + error);
    })
}

window.onload = function() {
    nextgeruecht();
    var button = document.getElementById('addgeruecht');
    button.addEventListener('click', function(event) {
        addgeruecht();
        event.preventDefault();
    })
    var geruecht = document.getElementById('geruecht-container');
    geruecht.addEventListener('click', function(event) {
        nextgeruecht();
        event.preventDefault();
    })
};
