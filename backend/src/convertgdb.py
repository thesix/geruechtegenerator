#!/usr/bin/env python3

'''
convert gdb from primary json format to current json format
'''

import json
import hashlib
import re
import time

oldgdb = json.load(open('../gdb.json', 'r'))
newgdb = {'rumor': [], 'user': [], 'name': [], 'meta': {}}
punctuation = re.compile('.*[.;:,!?]+\Z')
property = re.compile('{}')

def createId(string):
    string += str(time.time())
    return hashlib.sha256(string.encode('utf8')).hexdigest()

# convert rumors
for key in oldgdb['geruecht'].keys():
    meta = oldgdb['geruecht'][key]['meta']
    line = oldgdb['geruecht'][key]['sentence']
    properties = oldgdb['geruecht'][key]['properties']
    sentence = []
    propnum = 0
    numberofnames = 0

    for word in line.split():
        punct = False
        if punctuation.match(word):
            punct = word[-1:]
            word = word[:-1]
        if property.match(word):
            sentence.append(properties[propnum])
            line += properties[propnum]['type']
            if properties[propnum]['type'] == 'name':
                numberofnames += 1
            propnum += 1
        else:
            sentence.append({'type': 'static', 'word': word})
            line += word
        if punct:
            sentence.append({'type': 'punctuation', 'word': punct})

    meta['id'] = createId(line)
    meta['state'] = oldgdb['geruecht'][key]['state']
    meta['numberofnames'] = numberofnames
    meta['type'] = 'rumor'
    newgdb['rumor'].append({'value': sentence, 'meta': meta})

# convert names
for key in oldgdb['name'].keys():
    oldname = oldgdb['name'][key]
    newname = {'meta': oldname['meta']}
    newname['meta']['type'] = 'name'
    newname['meta']['id'] = createId(oldname['value'])
    newname['meta']['state'] = oldname['state']
    newname['value'] = oldname['value']
    newname['genetiv'] = oldname['genetiv']
    newname['gender'] = oldname['gender']
    newgdb['name'].append(newname)

# convert users
for key in oldgdb['user'].keys():
    newgdb['user'].append(oldgdb['user'][key])

newgdb['meta']['lastmod'] = time.time();
newgdb['meta']['modified'] = False
json.dump(newgdb, open('../newgdb.json', 'w'), indent = 2)
