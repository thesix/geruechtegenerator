# -*- coding: utf-8 -*-

'''

Copyright GPL here

'''

import random
import json
import os
import time
import hashlib
import re

# class Geruecht():
#     '''
#     one geruecht
#     '''
#
#     def __init__(self, data):
#         self.key = data['key']
#         self.sentence = data['sentence']
#         self.state = data['state']
#         self.properties = data['properties']
#         self.meta = data['meta']
#         self.names = []
#         self.num_names = 0
#         for p in self.properties:
#             if p['type'] == 'name':
#                 self.num_names += 1
#
#     def __render(self):
#         format_string = ()
#         for property in self.properties:
#             if property['type'] == 'word':
#                 format_string += (property[self.names[property['depends']].gender]),
#             elif property['type'] == 'name':
#                 n = self.names[property['order']].value
#                 if property['genetiv'] == True:
#                     n += self.names[property['order']].genetiv
#                 format_string += (n),
#
#         return self.sentence.format(*format_string)
#
#     def render(self):
#         return self.__render()
#
#     def export(self):
#         return {
#             'key': self.key,
#             'sentence': self.sentence,
#             'state': self.state,
#             'properties': self.properties,
#             'meta': self.meta,
#             }
#
# class GeruechtList(list):
#     '''
#     List of objects of class Geruecht
#     '''
#
#     def __init__(self, *args):
#         list.__init__(self, args)
#         self.key = 0
#
#     def getRandomGeruecht(self):
#         g = random.choice(self)
#         while g.state != itemstate.public:
#             g = random.choice(self)
#         g.names = self.nlist.getUniqueNames(g.num_names)
#         if len(g.names) != g.num_names:
#             return ''
#         return g.render()
#
#     def importGeruecht(self, data):
#         self.append(Geruecht(data))
#         if data['key'] > self.key:
#             self.key = data['key']
#
#     def addNewGeruecht(self, sentence, user):
#         now = time.time()
#         self.importGeruecht({
#             'key': self.key + 1,
#             'sentence': sentence,
#             'state': itemstate.private,
#             'properties': [],
#             'meta': {
#                 'added': now,
#                 'created': now,
#                 'lastmod': now,
#                 'added_by': user,
#                 'created_by': user,
#                 'lastmod_by': user,
#             },
#             })
#
#     def importName(self, data):
#         self.nl.importName(data)
#
# class Name:
#     '''
#     '''
#
#     def __init__(self, data):
#         self.key = data['key']
#         self.value = data['value']
#         self.gender = data['gender']
#         self.genetiv = data['genetiv']
#         self.state = data['state']
#         self.meta = data['meta']
#
#     def export(self):
#         return {
#             'key': self.key,
#             'value': self.value,
#             'gender': self.gender,
#             'genetiv': self.genetiv,
#             'state': self.state,
#             'meta': self.meta,
#         }
#
# class NameList(list):
#     '''
#     List of objects of class Name
#     '''
#
#     def __init__(self, *args):
#         list.__init__(self, args)
#         self.key = 0
#
#     def getUniqueNames(self, number):
#         if number > len(self):
#             return []
#
#         names = []
#         i = 0
#         while i < number:
#             name = random.choice(self)
#             if name not in names and name.state == itemstate.public:
#                 names.append(name)
#                 i += 1
#         return names
#
#     def importName(self, data):
#         self.append(Name(data))
#         if data['key'] > self.key:
#             self.key = data['key']
#
# class User(object):
#     '''
#     a user with rights inside gdb
#     '''
#     def __init__(self, data):
#         self.key = data['key']
#         self.name = data['name']
#         self.passwordhash = data['passwordhash']
#         self.lastseen = data['lastseen']
#
#     def export(self):
#         return {
#             'key': self.key,
#             'name': self.name,
#             'passwordhash': self.passwordhash,
#             'lastseen': self.lastseen,
#         }
#
# class UserList(list):
#     '''
#     '''
#     def __init__(self, *args):
#         list.__init__(self, args)
#         self.key = 0
#
#     def authenticate(self, name, password):
#         passwordhash = hashlib.sha256(password.encode('utf8')).hexdigest()
#         for user in self:
#             if user.name == name and user.passwordhash == passwordhash:
#                 return user
#         return None
#
#     def importUser(self, data):
#         self.append(User(data))
#         if data['key'] > self.key:
            # self.key = data['key']

class Gdb:
    '''
    '''

    def __init__(self, jsonfilename, log):
        # path to gdb.json
        self.jsonfilename = jsonfilename
        # log method
        self.log = log
        # buffer for renderd rumors
        self.buffer = []
        # threshold to refill buffer
        self.bufferfillthreshold = 2
        # bakcup copies of gdb.json to keep
        self.backupcopies = 8

    def loadJSON(self):
        self.data = json.load(open(self.jsonfilename, 'r'))
        self.log("loaded {} rumors, {} names, {} users".format(
            len(self.data['rumor']),
            len(self.data['name']),
            len(self.data['user']),
            )
        )

    def dumpJSON(self):
        if not self.data['meta']['modified']:
            self.log('Nothing to save')
            return
        self.backup()
        self.data['meta']['modified'] = False
        json.dump(self.data, open(self.jsonfilename, 'w'), indent = 2)

    def backup(self):
        for number in range(self.backupcopies, 0, -1):
            filename = '{}.{}'.format(self.jsonfilename, number)
            if not os.path.isfile(filename):
                continue
            nextfilename = '{}.{}'.format(self.jsonfilename, number+1)
            if number == self.backupcopies:
                os.unlink(filename)
                self.log('deleted {}'.format(filename))
            else:
                os.rename(filename, nextfilename)
                self.log('moved {} to {}'.format(filename, nextfilename))
        os.rename(self.jsonfilename, filename)
        self.log('moved {} to {}'.format(self.jsonfilename, filename))

    def getRandomRumor(self):
        rumor = random.choice(self.data['rumor'])
        while rumor['meta']['state'] != itemstate.public:
            rumor = random.choice(self.data['rumor'])
        return rumor

    def fillBuffer(self):
        if len(self.buffer) >= self.bufferfillthreshold:
            # self.log('buffersize {} is enough'.format(len(self.buffer)))
            return

        random.shuffle(self.data['rumor'])
        for rumor in self.data['rumor']:
            if rumor['meta']['state'] == itemstate.public:
                self.buffer.append(self.renderRumor(rumor))
        self.log('filled buffer.  Buffer now holds {} items'.format(len(self.buffer)))

    def getNextGeruecht(self):
        self.fillBuffer()
        next = self.buffer.pop(0)
        return next

    def createId(self, string):
        string += str(time.time())
        return hashlib.sha256(string.encode('utf8')).hexdigest()

    def addNewGeruecht(self, line, user):
        value = []
        pattern = re.compile('.*[.;:,!?]+\Z')
        for word in line.split():
            if pattern.match(word):
                value.append({'type': 'static', 'word': word[:-1]})
                value.append({'type': 'punctuation', 'word': word[-1:]})
            else:
                value.append({'type': 'static', 'word': word})

        now = time.time()
        string = ''
        for word in value:
            string += word['word']

        self.data['rumor'].append(
            {
                'value': value,
                'meta': {
                    'added': now,
                    'created': now,
                    'lastmod': now,
                    'added_by': user,
                    'created_by': user,
                    'lastmod_by': user,
                    'id': self.createId(string),
                    'state': itemstate.private,
                    'numberofnames': 0,
                    'type': 'rumor',
                },
            }
        )
        self.data['meta']['modified'] = True
        self.data['meta']['lastmod'] = time.time()

    def renderRumor(self, rumor):
        sentence = ''
        namelist = self.getUniqueNames(rumor['meta']['numberofnames'])
        if len(namelist) < rumor['meta']['numberofnames']:
            return sentence

        try:
            for word in rumor['value']:
                if word['type'] == 'static':
                    sentence += '{} '.format(word['word'])
                elif word['type'] == 'name':
                    if word['genetiv']:
                        sentence += '{}'.format(namelist[word['order']]['value'])
                        sentence += '{} '.format(namelist[word['order']]['genetiv'])
                    else:
                        sentence += '{} '.format(namelist[word['order']]['value'])
                elif word['type'] == 'word':
                    sentence += '{} '.format(word[namelist[word['namedependency']]['gender']])
                elif word['type'] == 'punctuation':
                    sentence = sentence[:-1]
                    sentence += '{} '.format(word['word'])
        except Exception:
            return 'Gerücht {} ist kaputt.'.format(rumor['meta']['id'])
        return sentence

    def getUniqueNames(self, number):
        if number > len(self.data['name']):
            return []

        names = []
        i = 0
        while i < number:
            name = random.choice(self.data['name'])
            if name not in names and name['meta']['state'] == itemstate.public:
                names.append(name)
                i += 1
        return names

    def authenticateUser(self, name, password):
        passwordhash = hashlib.sha256(password.encode('utf8')).hexdigest()
        for user in self.data['user']:
            if user['name'] == name and user['passwordhash'] == passwordhash:
                return user
        return None

class ItemState:
    def __init__(self):
        self.private = 0
        self.public = 1
        self.deleted = 2

itemstate = ItemState()

if __name__ == '__main__':
    import sys

    gdb = Gdb('../testgdb.json', print)
    gdb.loadJSON()

    # gdb.fillBuffer()
    for i in range(24):
        print(gdb.getNextGeruecht())
        time.sleep(0.1)

    gdb.addNewGeruecht('Irgendwer beabsichtigt, die Helmut-List-Halle zu kaufen.', 'anonymous')
    gdb.dumpJSON()

# EOF
