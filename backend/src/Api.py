import tornado.web
import tornado.ioloop
import tornado.httpserver
from time import strftime, time
import json
from libgg3 import Gdb
import os
import sys
from mimetypes import guess_type

def logmsg(msg):
    print('{}:  {}'.format(strftime('%c'), msg))
    sys.stdout.flush()


# location of the gdb json file
gdbfile = '/app/data/gdb/gdb.json'

# dir for static html files;  mainly needed for letsencrypt's certbot
basedir = '/app/data/www'

# url of frontend
# frontendurl = 'https://plagi.at/geruecht'
frontendurl = 'https://localhost'

# cookiesecret
cookiesecret = 'place-a-random-value-here';

class SessionList(list):
    def __init__(self, userlist):
        self.userlist = userlist
        self.timeout = 60 * 60

    def authenticate(self, name, password):
        user = gdb.authenticateUser(name, password)
        if not user:
            return None

        user['lastseen'] = time()
        self.append(user)
        # gdb.modified = True
        return user

    def validate(self, cookie):
        if not cookie:
            return None

        u = cookie.decode('utf8')
        for user in self:
            if user['name'] == u:
                if user['lastseen'] + self.timeout < time():
                    logmsg('session timeout for user={}'.format(u))
                    self.remove(user)
                    return None
                user['lastseen'] = time()
                return user
        logmsg('user={} not found'.format(u))
        return None

gdb = Gdb(gdbfile, logmsg)
gdb.loadJSON()
# gdb.fillBuffer()
sessionlist = SessionList(gdb.data['user'])

# a handler for static files
class FileHandler(tornado.web.RequestHandler):

    def set_default_headers(self):
        # while testing leave this open to anyone
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Methods', 'GET')

    @tornado.web.asynchronous
    def get(self, path):
        print('path={}'.format(path))
        if path == '':
            # we could also just serve a geruecht here
            path = 'index.html'
        location = os.path.join(basedir, path)
        if not os.path.isfile(location):
            raise tornado.web.HTTPError(status_code=404)
        content_type, _ = guess_type(location)
        self.add_header('Content-Type', content_type)
        with open(location) as source_file:
            self.finish(source_file.read())

class RestHandler(tornado.web.RequestHandler):

    @tornado.web.asynchronous
    def post(self, *args):
        pass

    @tornado.web.asynchronous
    def put(self, *args):
        pass

    @tornado.web.asynchronous
    def get(self):
        pass

    def options(self):
        self.set_status(204)
        self.finish()

class GeruechtHandler(RestHandler):
    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        # self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET')

    @tornado.web.asynchronous
    def get(self):
        geruecht = gdb.getNextGeruecht()
        if not geruecht:
            data = {'status': 'error'}
            logmsg('GeruechtHandler:  error remote_ip={}, referer={}, origin={}'.format(
                self.request.remote_ip, self.request.headers['Referer'], self.request.headers['Origin']))
        else:
            data = {'status': 'success', 'payload': geruecht}
            # here we will display the client's ip address
            try:
                logmsg('GeruechtHandler:  served geruecht to remote_ip={}, referer={}, origin={}'.format(
                    self.request.remote_ip, self.request.headers['Referer'], self.request.headers['Origin']))
            except KeyError:
                logmsg('GeruechtHandler:  served geruecht to remote_ip={}'.format(
                    self.request.remote_ip))
        data = json.dumps(data)
        self.finish(data)

class SessionHandler(RestHandler):
    '''
    check if a session exists
    '''

    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', frontendurl)
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, OPTIONS')
        self.set_header("Access-Control-Allow-Headers", 'Content-Type')

    def get(self):
        user = self.get_secure_cookie('user')
        if not user:
            data = {'status': 'error'}
            logmsg('no session cookie')
        else:
            data = gdb.data
            data['status'] = 'success'
            data['message'] = 'login successful'
            data['username'] = user.decode('utf8')
            logmsg('session cookie for user={} found'.format(user.decode('utf8')))

        self.finish(json.dumps(data))


class AddHandler(RestHandler):
    '''
    Handler to let users add new rumors
    '''

    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', frontendurl)
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'OPTIONS, PUT')
        self.set_header("Access-Control-Allow-Headers", 'Content-Type')

    @tornado.web.asynchronous
    def put(self):
        user = self.get_secure_cookie('user')
        if not user:
            user = 'anonymous'

        data = tornado.escape.json_decode(self.request.body)
        if data['type'] == 'name':
            self.finish(json.dumps({'status': 'error', 'message': 'not implemented yet'}))
            logmsg('AddHandler:  adding new name not implemented yet')
            return

        if data['type'] == 'geruecht':
            if len(data['payload']) < 20:
                self.finish(json.dumps({'status': 'error', 'message': 'Fehler!  Gerücht zu kurz.'}))
                logmsg('AddHandler:  refused to add new geruecht (too short)')
                return

            gdb.addNewGeruecht(data['payload'], user.decode('utf8'))
            self.finish(json.dumps({'status': 'success', 'message': 'Gerücht hinzugefügt.'}))
            logmsg('AddHandler:  added new geruecht from remote_ip={}'.format(self.request.remote_ip))
            return

        # fallthrough
        self.finish(json.dumps({'status': 'error', 'message': 'unknown type'}))

class LoginHandler(RestHandler):
    '''
    handle login for rumor editors
    '''

    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', frontendurl)
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'POST, OPTIONS')

    @tornado.web.asynchronous
    def post(self, name, password):
        user = sessionlist.authenticate(name, password)
        if not user:
            self.finish(json.dumps({'status': 'error'}))
            logmsg('LoginHandler:  authentication error for user={}, password={}'.format(name, password))
        else:
            data = gdb.data
            data['status'] = 'success'
            data['message'] = 'login successful'
            self.set_secure_cookie('user', user['name'])
            self.finish(json.dumps(data))
            logmsg('LoginHandler:  user={} logged in'.format(user['name']))
            # for key in data.keys():
            #     logmsg(key)

# class ListHandler(RestHandler):
#     def set_default_headers(self):
#         # while testing leave this open to anyone
#         self.set_header('Access-Control-Allow-Origin', frontendurl)
#         self.set_header('Access-Control-Allow-Credentials', 'true')
#         self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT')
#
#     @tornado.web.asynchronous
#     def get(self, listobject):
#         user = sessionlist.validate(self.get_secure_cookie('name'))
#         if not user:
#             self.finish(json.dumps({'status': 'error', 'message': 'unauthorized access'}))
#             logmsg('ListHandler:  unauthorized access')
#             return
#
#         # payload = gdb.getdict(listobject)
#         # if not payload:
#         #     self.finish(json.dumps({'status': 'error', 'message': 'unknown getlist type'}))
#         #     logmsg('ListHandler:  unknown getlist type {}'.format(listobject))
#         #     return
#
#         data = {'status': 'success', 'payload': payload,'message': len(payload), }
#         logmsg('ListHandler: list type={}, user={}'.format(listobject, user.name))
#         self.finish(json.dumps(data))

class SaveHandler(RestHandler):
    '''
    save gdb json object
    '''

    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', frontendurl)
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'OPTIONS, PUT')
        self.set_header("Access-Control-Allow-Headers", 'Content-Type')

    @tornado.web.asynchronous
    def put(self):
        user = sessionlist.validate(self.get_secure_cookie('user'))
        if not user:
            self.finish(json.dumps({'status': 'error'}))
            logmsg('SaveHandler: unauthorized access')
            return


        # data = tornado.escape.json_decode(self.request.body)

        logmsg('SaveHandler: not implemented yet')
        self.finish(json.dumps({
            'status': 'error',
            'message': 'Sorry {}.  SaveHandler not implemented yet'.format(user.name)
            }))

def run():
    if len (gdb.data['rumor']) == 0:
        logmsg('error: there are 0 geruechte.  Exiting.')
        sys.exit(1)

    if len(gdb.data['name']) == 0:
        logmsg('error:  there are 0 names.  Exiting.')
        sys.exit(2)

    if len(gdb.data['user']) == 0:
        logmsg('warning:  there are 0 users')

    logmsg('info:  loaded {} rumors and {} names'.format(
        len(gdb.data['rumor']), len(gdb.data['name'])
    ))
    print(gdb.data)
    sys.stdout.flush()

    handlers = [
        tornado.web.url(r'/get', GeruechtHandler),
        tornado.web.url(r'/add', AddHandler),
        tornado.web.url(r'/login/user/([A-Za-z0-9]+)/password/([A-Za-z0-9]+)', LoginHandler),
        tornado.web.url(r'/session', SessionHandler),
        tornado.web.url(r'/save', SaveHandler),
        tornado.web.url(r'/(.*)', FileHandler),
    ]
    settings = dict(
        cookie_secret = cookiesecret,
        login_url = '/login/user/anon/password/none',
    )

    app = tornado.web.Application(handlers, **settings)

    # server = tornado.httpserver.HTTPServer(app,
    #         ssl_options = {
    #             'certfile': '/app/data/ssl/fullchain.pem',
    #             'keyfile': '/app/data/ssl/privkey.pem',
    #             })

    server = tornado.httpserver.HTTPServer(app)
    try:
        server.listen(80)
        logmsg('App ready')
        tornado.ioloop.IOLoop.instance().start()

    except KeyboardInterrupt:
        logmsg('App finished')
        gdb.dumpJSON()

if __name__ == '__main__':
    pass
