#!/usr/bin/env python3

import sys
sys.path.append('/app/src')
import Api

if __name__ == '__main__':
    Api.run()
